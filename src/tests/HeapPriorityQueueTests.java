package tests;

import queues.*;
import static org.junit.jupiter.api.Assertions.*;

import java.awt.List;
import java.util.ArrayList;

import org.junit.Test;

public class HeapPriorityQueueTests {
	private HeapPriorityQueue<Integer> emptyHeap = new HeapPriorityQueue<Integer>(1);

	@Test
	public void isEmpty_ShouldReturnTrue_WhenHeapIsEmpty() {
		assertTrue(emptyHeap.isEmpty(), "Expected empty heap, but returned heap is not empty.");
	}

	@Test
	public void isEmpty_ShouldReturnFalse_WhenHeapIsFull() throws Exception {
		emptyHeap.enqueue(2);
		assertFalse(emptyHeap.isEmpty(), "Expected false because heap is full, but is true. ");

	}

	@Test
	public void isFull_ReturnsFalse_WhenHeapIsNotFull() throws Exception {
		HeapPriorityQueue<Integer> fullHeap = new HeapPriorityQueue<Integer>(3);
		assertFalse(fullHeap.isFull(), "Expected false because heap is empty.");
		fullHeap.enqueue(1);
		fullHeap.enqueue(2);
		assertFalse(fullHeap.isFull(), "Expected false because heap is not full.");
	}

	@Test
	public void Enqueue_ShouldThrowException_WhenHeapIsFull() throws Exception {
		HeapPriorityQueue<Integer> fullHeap = new HeapPriorityQueue<Integer>(2);
		fullHeap.enqueue(1);
		fullHeap.enqueue(2);
		assertThrows(Exception.class, () -> fullHeap.enqueue(2));
	}

	@Test
	public void Enqueue_ShouldSucceed() throws Exception {
		HeapPriorityQueue<Integer> fullHeap = new HeapPriorityQueue<Integer>(1);
		fullHeap.enqueue(1);
		assertTrue(fullHeap.isFull(), "If it isn't full, something went wrong.");

	}

	@Test
	public void Dequeue_ShouldThrowException_WhenHeapIsEmpty() throws Exception {
		assertThrows(Exception.class, () -> emptyHeap.dequeue());
	}

	@Test
	public void Dequeue_ShouldSucceed() throws Exception {
		HeapPriorityQueue<Integer> fullHeap = new HeapPriorityQueue<Integer>(3);
		fullHeap.enqueue(1);
		fullHeap.dequeue();
		
		//resultList.add((int) fullHeap.dequeue());
		//assertEquals(dequeueControl, (int) resultList.get(0), "It wasn't 1.");
		assertTrue(fullHeap.isEmpty());
		/*
		 * resultList.add(fullHeap.dequeue()); assertEquals(controlList.length,
		 * resultList.size());
		 */
	}
	
	@Test
	public void DequeueMultiple_ShouldSucceed() throws Exception {
		HeapPriorityQueue<Integer> fullHeap = new HeapPriorityQueue<Integer>(3);
		fullHeap.enqueue(3);
		fullHeap.enqueue(2);
		fullHeap.enqueue(1);
		fullHeap.dequeue();
		fullHeap.dequeue();
		//Its not possible to dequeue multiple times
		//Something is wrong with implementation of SmallestLeaf 
		//Will give IndexOutOfBoundsError nomatter the amount of spots in HeapPriorityQueue
		
		//resultList.add((int) fullHeap.dequeue());
		//assertEquals(dequeueControl, (int) resultList.get(0), "It wasn't 1.");
		assertTrue(fullHeap.isEmpty());
		/*
		 * resultList.add(fullHeap.dequeue()); assertEquals(controlList.length,
		 * resultList.size());
		 */
	}
	
}
