package tests;

import queues.StubbQueue;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

class StubbQueueTest {
    StubbQueue<Integer> integerQueue = new StubbQueue<>();
    StubbQueue<Integer> emptyQueue = new StubbQueue<>();

    public void startUp() {
    	   integerQueue.enqueue(4);
           integerQueue.enqueue(5);
           integerQueue.enqueue(3);
           integerQueue.enqueue(2);
    }
    
    //Illegal operations
    @Test
    void illegalClear() {
    	this.startUp();
        assertEquals(false, emptyQueue.isEmpty());
    }
    @Test
    void illegalGetFront(){
        assertEquals(false, emptyQueue.getFront() );
    }
    @Test
    void illegalGetSize(){
        assertEquals(false, emptyQueue.getSize() );
    }
    @Test
    void illegalOpOnEmptyQueue(){
        assertEquals(false, emptyQueue.dequeue() );
    }
    @org.junit.jupiter.api.Test
    void illegalOpOnFullQueue(){}

    //Legal operations
    @org.junit.jupiter.api.Test
    void clear() {
        assertEquals(true, integerQueue.isEmpty());
    }

    @org.junit.jupiter.api.Test
    void isEmpty() {
        assertEquals(true, emptyQueue.isEmpty());
    }

    @org.junit.jupiter.api.Test
    void isFull() {
        assertEquals(true, integerQueue.isFull());
    }

    @org.junit.jupiter.api.Test
    void getSize() {
        assertEquals(4, integerQueue.getSize());
    }

    @org.junit.jupiter.api.Test
    void enqueue() {
        this.integerQueue.enqueue(10);
        assertEquals(10, integerQueue.dequeue());
    }

    @org.junit.jupiter.api.Test
    void dequeue() {
        assertEquals(4, integerQueue.dequeue());
        assertEquals(5, integerQueue.dequeue());
    }

    @org.junit.jupiter.api.Test
    void getFront() {
        assertEquals(4, integerQueue.getFront());
    }
}