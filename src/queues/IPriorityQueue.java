package queues;
public interface IPriorityQueue<T extends Comparable<? super T>> {

	
    void clear();
    Boolean isEmpty();
    Boolean isFull();
    int getSize();
    void enqueue(T object) throws Exception;
    T dequeue() throws Exception;
    T getFront() throws Exception; // possibly remove
}
