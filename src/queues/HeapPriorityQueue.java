package queues;
//import java.util.ArrayList;

public class HeapPriorityQueue <T extends Comparable<? super T>> implements IPriorityQueue<T> {
	private T[] priorityQueue;
	private int currentSize;
	private int size;
	private int write;

	@SuppressWarnings("unchecked")
	public HeapPriorityQueue(int size) {
		this.priorityQueue = (T[]) (new Comparable[size]);
		priorityQueue[0] = (T) (Integer.valueOf(1));
		this.size = size;
		this.currentSize = 0;
	}

	/**
	 * Clears the queue. 
	 * 
	 */
	@Override
	public void clear() {
		currentSize = 0;
		write = 0;
	}

	/**
	 * Shows whether or not the queue is empty
	 * @return Boolean depending on whether the queue is empty or not.
	 */
	@Override
	public Boolean isEmpty() {
		return (currentSize == 0);
	}

	/**
	 * Shows whether or not the queue is empty.
	 * @return Boolean depending on whether the queue is full or not.
	 */
	@Override
	public Boolean isFull() {
		return (currentSize == size);
	}

	/**
	 * Gets the size of the total queue.
	 * @return The integer size of the total queue..
	 */
	@Override
	public int getSize() {
		return currentSize;
	}

	/**
	 * 
	 * @param enqueue The object to be queued.
	 */
	@Override
	public void enqueue(T object) throws Exception {
		if(priorityQueue == null) {
			throw new Exception("EMPTY Priority queue.");
		}
		priorityQueue[write] = object;
		write++;
		currentSize++;
		recBubbleUp(write - 1);
	}

	/**
	 * 
	 * @return The object removed from queue. 
	 * @throws Exception
	 */
	@Override
	public T dequeue() throws Exception {
		if (isEmpty()) {
			throw new Exception("Empty Priority queue");
		}
		T temp = priorityQueue[0];
		currentSize--;
		if (!isEmpty()) {
			priorityQueue[0] = priorityQueue[currentSize - 1];
			recBubbleDown(0);
		}	

		return temp;
	}

	@Override
	public T getFront() {
		return null;

	}
	/**
	 * 
	 * @param childPosition The current position of the child.
	 * @return The position of the parent
	 */
	private int getParent(int childPosition) {
		return (childPosition - 1) / 2;
	}
	
	/**
	 * 
	 * @param parentPosition The current position of the parent.
	 * @return The position of the left leaf.
	 */
	private int getLeftLeaf(int parentPosition) {
		return ((parentPosition + 1) / 2) - 1;
	}
	
	/**
	 * 
	 * @param parentPosition The current position of the parent.
	 * @return The position of the right leaf.
	 */
	private int getRightLeaf(int parentPosition) {
		return ((parentPosition + 1) / 2);
	}

	/**
	 * Recursive method for bubbling up enqueued objects to right position.
	 * @param position
	 */
	private void recBubbleUp(int position) {
		if (position != 0) {

		} else {
			if (position > getParent(position)) {
				// ToDo
			}
			if (position < getParent(position)) {
				T temp = priorityQueue[getParent(position)];
				priorityQueue[getParent(position)] = priorityQueue[position];
				priorityQueue[position] = temp;
				recBubbleUp(getParent(position));
			}
		}
	}
	
	/**
	 * Recursive method for bubbling up enqueued objects to right position.
	 * @param position
	 */
	private void recBubbleDown(int position) {
		int smallest = smallestLeaf(position); //Index error here
		if (smallest != position) {
			T temp = priorityQueue[position];
			priorityQueue[position] = priorityQueue[smallest];
			priorityQueue[smallest] = temp;
			recBubbleDown(smallest);
		}
	}

	/**
	 * 
	 * @param position The current position.
	 * @return The position of the smallest leaf.
	 */
	private int smallestLeaf(int position) {
		int result = 0;
		T parent = priorityQueue[position];

		if (getSize() < getLeftLeaf(position)) {
			if (parent.compareTo(priorityQueue[getLeftLeaf(position)]) < 0) {
				result = position;
			} else {
				result = getLeftLeaf(position);
			}
		} else {
			System.out.println(position);
			T left = priorityQueue[getLeftLeaf(position)];
			T right = priorityQueue[getRightLeaf(position)];

			if (parent.compareTo(left) < 0 && parent.compareTo(right) < 0) {
				result = getParent(getLeftLeaf(position));
			}
			if (parent.compareTo(left) < 0 && parent.compareTo(right) > 0) {
				result = getRightLeaf(position);
			}
			if (parent.compareTo(left) > 0 && parent.compareTo(right) > 0) {
				if (left.compareTo(right) < 0) {
					result = getLeftLeaf(position);
				} else {
					result = getRightLeaf(position);
				}
			}
		}
		return result; //This is horribly wrong. 
	}
}
