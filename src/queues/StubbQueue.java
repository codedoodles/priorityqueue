package queues;
/**
 * 
 * @author 18anaa01
 *
 * @param <T> A Queue that accepts comparable types. 
 */

public class StubbQueue<T> implements IPriorityQueue{
    @Override
    public void clear() {

    }

    @Override
    public Boolean isEmpty() {
        return null;
    }

    @Override
    public Boolean isFull() {
        return null;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public void enqueue(Comparable object) {

    }

    @Override
    public Comparable dequeue() {
        return null;
    }

    @Override
    public Comparable getFront() {
        return null;
    }
}
